            #region Character Creation
            { PlayerMessages.CHARACTER_NAME_INVALID, "Prueba otro nombre!" },
            { PlayerMessages.CHARACTER_NAME_ALREADY_TAKEN, "El nombre del personaje ya fue escogido" },
            #endregion
            #region Upgrade
            { PlayerMessages.UPGRADE_RARIFY_SUCCESS,"Apuesta correcta!"},
            # endregion
            #region Skills
            { PlayerMessages.SKILL_YOU_LEARNED_SKILL_X,"Habilidad {0} aprendida correctamente"},
            #endregion
            # region Changements
            { PlayerMessages.CHARACTER_YOUR_CLASS_CHANGED_TO_X,"Has cambiado tu clase a {0}"},
            { PlayerMessages.CHARACTER_X_CLASS_CHANGED_TO_Y,"Has cambiado de la clase {0} a la clase {1} "},
            { PlayerMessages.CHARACTER_YOUR_GENDER_CHANGED_TO_X,"Has cambiado tu género a {0}"}
            { PlayerMessages.CHARACTER_X_GENDER_CHANGED_TO_Y,"Has cambiado tu genero de {0} a {1}"},
            { PlayerMessages.CHARACTER_YOUR_FACTION_CHANGED_TO_X,"Has cambiado tu faccion a {0}"},
            { PlayerMessages.CHARACTER_X_FACTION_CHANGED_TO_Y,"Has cambiado tu faccion de {0} a {1}"},
            # endregion
            # region Family
            { PlayerMessages.FAMILY_PLAYERX_HAS_ALREADY_FAMILY,"No puedes invitar a {0} a tu familia por que ya tiene una familia!"},
            { PlayerMessages.FAMILY_ALREADY_LEADER,"Ya eres el lider de la familia!"},
            { PlayerMessages.FAMILY_PLAYERX_JOINED_FAMILYX,"{0} se ha unido a la familia!"},
            { PlayerMessages.FAMILY_YOU_NEED_TO_BE_LEADER,"Necesitas ser el lider de la familia!"},
            # endregion
            # region You don't have requirements
            { PlayerMessages.YOU_DONT_HAVE_ENOUGH_GOLD,"No tienes suficiente oro para hacer eso!"},
            { PlayerMessages.YOU_DONT_HAVE_ENOUGH_REPUTATION,"No tienes suficiente reputación para hacer eso!"},
            { PlayerMessages.YOU_DONT_HAVE_ENOUGH_SPACE_IN_INVENTORY,"No tienes suficiente espacio en tu inventario!"},            
            # endregion
            # region Relations
            { PlayerMessages.FRIEND_X_INVITED_YOU_TO_JOIN_HIS_FRIENDLIST,"{0} te ha enviado una petición de amistad "},
            { PlayerMessages.FRIEND_X_IS_NOW_IN_YOUR_FRIENDLIST,"{0} es ahora tu amigo!"},
            # endregion
            # region Pets
            { PlayerMessages.PETS_YOU_GET_X_AS_A_NEW_PET,"Enhorabuena,{0} es ahora tu nueva mascota !"},
            { PlayerMessages.PETS_YOU_GET_X_AS_A_NEW_PARTNER,"Enhorabuena,{0} es ahora tu nuevo compañero !"},
            # endregion
            # region Group
            { PlayerMessages.GROUP_PLAYER_X_INVITED_YOU_TO_JOIN_HIS_GROUP, "{0} te ha invitado a unirte a su grupo"},
            { PlayerMessages.GROUP_PLAYER_X_INVITED_TO_YOUR_GROUP, "Has invitado a {0} a unirse a tu grupo"},
            { PlayerMessages.GROUP_PLAYER_X_JOINED_YOUR_GROUP, "{0} se ha unido a tu grupo" },
            { PlayerMessages.GROUP_YOU_JOINED_GROUP_OF_PLAYER_X, "te has unido al grupo de {0}" },
            # endregion
